{ unstable, channels, ... }:
final: prev: {
  inherit (channels.unstable) nvidia_x11;
}
