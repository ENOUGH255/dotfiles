{
  pkgs,
  config,
  lib,
  nixos-hardware,
  nixosModules,
  ...
}:
with lib;
with lib.campground; let
  newUser = name: {
    isNormalUser = true;
    createHome = true;
    home = "/home/${name}";
    shell = pkgs.zsh;
  };
in {
  imports = [./hardware.nix];

  campground = {
    archetypes.barebones = enabled;

    system = {
      boot = enabled;
      zfs = {
        enable = true;
        hostId = "13ec383b"; # run -> head -c 8 /dev/machine-id
        keyfile-url = "http://10.8.0.1:1234/zfs-keyfile"; # optional for autounlocking
      };
      passwds = enabled;
    };

    user = {
      name = "abe";
      fullName = "Matt Camp";
      email = "matt@aicampground.com";
      extraGroups = ["wheel"];
    };

    services = {
      openssh = {
        enable = true;
        authorizedKeys = [
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGw+o+9F4kz+dYyI2I4WudgKjyFOK+L0QW4LhxkG4sMt gitlab-runner@aicampground.com"
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKdMWMFyi7Lvjm78KOX3tKZ5bkEZ7bHA56ZKKtTb9wIo mcamp@aicampground.com"
        ];
      };
      ntp = enabled;
    };
  };

  system.stateVersion = "23.05";
}
